var globalId;
$(function(){
	queryList();
	$("#saveBtn").click(function(){
		return save();
	});
	$("#editBtn").click(function(){
		update();
	});
	$("#delBtn").click(function(){
		return del();
	});
});

function queryList(){
	$.ajax({
		url: "book/queryList",
		type:"get",
		dataType:"json",
		success:function(books){
				$("#tableId tbody").html("");
				if(books != null){
				$(books).each(function(n, value){
					//n:循环的是第几个元素的下标，从0开始
					//value:循环的当前元素，就是role对象 。
						var tr = 
						  '<tr>'+
			              '<td>'+value.bookId+'</td>'+
			              '<td>'+value.bookName+'</td>'+
			              '<td>'+
			              '<a href="" onclick=updateClick("'+value.bookId+'","'+value.bookName+'") id="update'+value.bookId+'" data-toggle="modal" data-target="#editRole" ><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>编辑</a>'+
			              '<a href="" onclick=deleteClick("'+value.bookId+'") id="del'+value.bookId+'" data-toggle="modal" data-target=".bs-example-modal-sm"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>删除</a>'+
			              '</td>'+
			              '</tr>';
						$("#tableId tbody").append(tr);
				});
		}else{
			alert("库中无数据")
		}
		},
		error: function(){
			alert("请求失败！")
		}
	});
}
function updateClick(id, name){
	$("#editInput").val(name);
	globalId=id;
}
function save(){
	var bookName = $("#bookName").val();
	$.ajax({
		url: "book/add",
		contentType: 'application/json',
		type:"post",
		data:JSON.stringify({"bookName": bookName}),
		dataType:"json",
		success:function(status){
			alert("添加成功");
			$("#menu").click();
		},
		error: function(){
			alert("请求失败！")
		}
	});
	return false;
}
function update(){
	var name = $("#editInput").val();
	$.ajax({
		url: "book/update",
		contentType: 'application/json',
		type:"post",
		data:JSON.stringify({"bookId": parseInt(globalId), "bookName": name}),
		dataType:"json",
		success:function(status){
			alert("更新成功");
		},
		error: function(){
			alert("请求失败！")
		}
	});
	return false;
}

function deleteClick(id){
	globalId = id;
}
function del(){
	$.ajax({
		url: "book/delete",
		contentType: 'application/json',
		type:"delete",
		data:JSON.stringify({"bookId": parseInt(globalId)}),
		dataType:"json",
		success:function(status){
			alert("删除成功");
		},
		error: function(){
			alert("请求失败！")
		}
	});
	return false;
}
