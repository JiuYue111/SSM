package com.tarena.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tarena.bean.Book;
import com.tarena.service.BookService;

@RestController
@RequestMapping("/book")
public class BookController {
	@Autowired
	private BookService bookService;
	@RequestMapping(value = "/queryList", method=RequestMethod.GET)
	public List<Book> queryList(){
		return bookService.queryList();
	}
	
	@RequestMapping(value = "/add", method=RequestMethod.POST)
	public int add(@RequestBody Book book){
		System.out.println(book);
		return bookService.add(book);
	}
	@RequestMapping(value = "/update", method=RequestMethod.POST)
	public int update(@RequestBody Book book){
		System.out.println(book);
		return bookService.update(book);
	}
	@RequestMapping(value = "/delete", method=RequestMethod.DELETE)
	public int delete(@RequestBody Book book){
		System.out.println(book);
		return bookService.delete( book);
	}
	
	

}
