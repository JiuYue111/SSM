package com.tarena.service;

import java.util.List;

import com.tarena.bean.Book;

public interface BookService {
	public List<Book> queryList();
	public int add(Book book);
	public int update(Book book);
	public int delete(Book book);
}
