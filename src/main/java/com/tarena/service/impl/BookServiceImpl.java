package com.tarena.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tarena.bean.Book;
import com.tarena.dao.BookMapper;
import com.tarena.service.BookService;

@Service
public class BookServiceImpl implements BookService{
	@Autowired
	private BookMapper bookMapper;

	@Override
	public List<Book> queryList() {
		return bookMapper.queryList();
	}

	@Override
	public int add(Book book) {
		return bookMapper.add(book);
	}

	@Override
	public int update(Book book) {
		return bookMapper.update(book);
	}

	@Override
	public int delete(Book book) {
		return bookMapper.delete(book);
	}
	
}

