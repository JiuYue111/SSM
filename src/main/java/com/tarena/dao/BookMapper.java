package com.tarena.dao;

import java.util.List;

import com.tarena.bean.Book;

public interface BookMapper {
	public List<Book> queryList();
	public int add(Book book);
	public int update(Book book);
	public int delete(Book book);
}
